import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Home from './pages/Home';
import Login from './pages/Login';
import PrivateRoute from './components/PrivateRoute'

export default function App() {
  return (
    <Router>
      <Switch>
        <PrivateRoute authed={localStorage.getItem('token') !== null} exact path='/' component={Home} />
        <Route path='/login' component={Login} />
      </Switch>
    </Router>
  );
}
