import React, { useState } from 'react'
import Navbar from '../components/Navbar'
import UserTable from '../components/UserTable';
import AddUser from '../components/AddUser';
import MakePayment from '../components/MakePayment';
import UsersDue from '../components/UsersDue';

export default function Home() {
    const [section, setSection] = useState(0);
    const [editing, setEditing] = useState(false);
    const [editUserId, setEditUserId] = useState('');

    const RenderComponents = () => {
        switch (section) {
            case 0: return <UserTable setEditing={setEditing} setEditUserId={setEditUserId} setSection={setSection} />
            case 1: return <AddUser editing={editing} setEditing={setEditing} editUserId={editUserId} setSection={setSection} />
            case 2: return <UsersDue />
        }
    }

    return (
        <div>
            <Navbar section={section} setSection={setSection} setEditing={setEditing} />
            <RenderComponents />
        </div>
    )
}
