import React, { useEffect, useState } from 'react';
import { ArrowLeftIcon } from '@heroicons/react/outline'
import { BASE_URL } from '../config';

export default function MakePayment({ userId, username, setMakePayment }) {
    const [values, setValues] = useState({
        amountDue: ''
    });
    const [error, setError] = useState(null);
    const [alert, setAlert] = useState(false);

    const { amountDue } = values

    const handleChange = (e) => {
        e.preventDefault();
        const { name, value } = e.target;
        setValues({
            ...values,
            [name]: value
        });
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        makePayment();
    }

    const makePayment = () => {
        fetch(`${BASE_URL}/users/${userId}/payment`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(values)
        }).then(response => {
            if (response.status !== 200) {
                setError(response.status + '===>' + response.statusText + '===>' + response.url)
            }
            setError(null);
            return response.json();
        }).then(data => {
            setAlert(true);
            setMakePayment(false);
        });
    }

    return (
        <div className="mt-10 sm:mt-0 p-16">
            <div className="mt-5 md:mt-0 md:col-span-2">
                <div className="m-5 flex items-center cursor-pointer" onClick={() => setMakePayment(false)}>
                    <ArrowLeftIcon className="w-4 h-4 mr-3" />
                    <p className="block text-sm font-medium text-gray-700">Go Back</p>
                </div>
                <form action="#" onSubmit={handleSubmit}>
                    <div className="shadow overflow-hidden sm:rounded-md">
                        <div className="px-4 py-5 bg-white sm:p-6">
                            <div className="grid grid-cols-6 gap-6">

                                <div className="col-span-6 sm:col-span-3">
                                    <label htmlFor="amount-due" className="block text-sm font-medium text-gray-700 mb-1">
                                        Amount Due
                                    </label>
                                    <input
                                        type="number"
                                        name="amountDue"
                                        id="amount-due"
                                        value={amountDue}
                                        onChange={handleChange}
                                        className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                            <button
                                type="submit"
                                className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                            >
                                Make Payment for {username}
                            </button>
                        </div>
                    </div>
                </form>
                {alert && (
                    <div class="mt-10 bg-blye-100 border border-blue-400 text-blue-700 px-4 py-3 rounded relative" role="alert">
                        <strong class="font-bold">Success!</strong>
                        <span class="absolute top-0 bottom-0 right-0 px-4 py-3" onClick={() => setAlert(false)}>
                            <svg class="fill-current h-6 w-6 text-blue-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" /></svg>
                        </span>
                    </div>
                )}
            </div>
        </div>
    )
}
