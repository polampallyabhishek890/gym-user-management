import React, { useEffect, useState } from 'react';
import { ChevronLeftIcon, ChevronRightIcon, SearchIcon, ArrowCircleRightIcon } from '@heroicons/react/solid'
import { BASE_URL } from '../config';
import MakePayment from './MakePayment';

export default function UserTable({ setEditing, setEditUserId, setSection }) {
  const [users, setUsers] = useState([])
  const [error, setError] = useState(null)
  const [page, setPage] = useState(1)
  const [details, setDetails] = useState({
    count: 0,
    totalPages: 0
  })
  const [makePayment, setMakePayment] = useState(false)
  const [makePaymentUser, setMakePaymentUser] = useState(null)
  const limit = 10
  const [searchValue, setSearchValue] = useState('')

  useEffect(() => {
    fetchUsers()
  }, [page, makePayment]);

  const fetchUsers = () => {
    fetch(`${BASE_URL}/users?page=${page}&limit=${limit}&search=${searchValue}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
    }).then(response => {
      if (response.status !== 200) {
        setError(response.status + '===>' + response.statusText + '===>' + response.url)
      }
      setError(null);
      return response.json();
    }).then(data => {
      setUsers(data.users);
      setDetails({
        count: data.count,
        totalPages: data.totalPages
      });
    });
  }

  const cancelSubscription = (id) => {
    fetch(`${BASE_URL}/users/${id}/cancel`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
    }).then(response => {
      if (response.status !== 200) {
        setError(response.status + '===>' + response.statusText + '===>' + response.url)
      }
      setError(null);
      return response.json();
    }).then(data => {
      fetchUsers();
    });
  }

  const renewSubscription = (id) => {
    fetch(`${BASE_URL}/users/${id}/renew`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
    }).then(response => {
      if (response.status !== 200) {
        setError(response.status + '===>' + response.statusText + '===>' + response.url)
      }
      setError(null);
      return response.json();
    }).then(data => {
      fetchUsers();
    });
  }

  const updatePaymentDue = (id) => {
    fetch(`${BASE_URL}/payments/${id}`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
    }).then(response => {
      if (response.status !== 200) {
        setError(response.status + '===>' + response.statusText + '===>' + response.url)
      }
      setError(null);
      return response.json();
    }).then(data => {
      fetchUsers();
    });
  }

  if (makePayment && makePaymentUser !== null) {
    return <MakePayment userId={makePaymentUser._id} username={makePaymentUser.name} setMakePayment={setMakePayment} />
  } else {
    return (
      <div className="flex flex-col py-16 px-10">
        <div className="flex items-center my-5 relative" style={{ width: '30%' }}>
          <input class="pl-10 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="Search" value={searchValue} onChange={e => setSearchValue(e.target.value)} />
          <SearchIcon className="h-5 w-5 absolute top-2 left-2.5 text-gray-500" aria-hidden="true" />
          <ArrowCircleRightIcon className="h-10 w-10 ml-4 cursor-pointer text-indigo-500" aria-hidden="true" onClick={() => fetchUsers()} />
        </div>
        {users.length === 0 ? (
          <div class="mt-10 bg-blye-100 border border-blue-400 text-blue-700 px-4 py-3 rounded relative" role="alert">
            <strong class="font-bold">No Users found.</strong>
            <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
              <svg class="fill-current h-6 w-6 text-blue-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" /></svg>
            </span>
          </div>
        ) : (
          <>
            <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                  <table className="min-w-full divide-y divide-gray-200">
                    <thead className="bg-gray-50">
                      <tr>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Name
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Phone Number
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Payment Plan
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Payment
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Joined
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Status
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Due
                        </th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase">
                          <span className="sr-only">Edit</span>
                        </th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase">
                        </th>
                      </tr>
                    </thead>
                    <tbody className="bg-white divide-y divide-gray-200">
                      {users.map((user, idx) => (
                        <tr key={idx}>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <div className="flex items-center">
                              {/* <div className="flex-shrink-0 h-10 w-10">
                              <img className="h-10 w-10 rounded-full" src={person.image} alt="" />
                            </div> */}
                              <div>
                                <div className="text-sm font-medium text-gray-900">{user.name}</div>
                                <div className="text-sm text-gray-500">{user.email}</div>
                              </div>
                            </div>
                          </td>
                          <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{user.phone}</td>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <div className="text-sm text-gray-900">Rs.{user.paymentPlan}</div>
                            <div className="text-sm text-gray-500">{user.paymentCycle} Months</div>
                          </td>
                          <td className="px-6 py-4 whitespace-nowrap">
                            {user.nextPaymentDate < new Date() && updatePaymentDue(user._id)}
                            <div className="text-sm text-gray-900">Upcoming: {(new Date(user.nextPaymentDate)).toLocaleDateString()}</div>
                            <div className="text-sm text-gray-500">Previous: {(new Date(user.lastPaymentDate)).toLocaleDateString()}</div>
                          </td>
                          <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{(new Date(user.startDate)).toLocaleDateString()}</td>
                          <td className="px-6 py-4 whitespace-nowrap">
                            {user.active ? (
                              <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                Active
                              </span>
                            ) : (
                              <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">
                                Inactive
                              </span>
                            )}
                          </td>
                          {user.amountDue > 0 ? (
                            <td className="px-6 py-4 whitespace-nowrap">
                              <div className="text-sm text-gray-900">Rs.{user.amountDue}</div>
                              <div className="text-sm text-red-400 cursor-pointer" onClick={() => {
                                setMakePaymentUser(user)
                                setMakePayment(true)
                              }}>Make Payment</div>
                            </td>
                          ) : (
                            <td className="px-6 py-4 whitespace-nowrap">
                              <div className="text-sm text-purple-400">All cleared</div>
                            </td>
                          )}
                          <td className="px-6 py-4 whitespace-nowrap">
                            {user.active ? (
                              <button class="bg-blue-500 text-xs hover:bg-blue-700 text-white py-2 px-4 rounded" onClick={() => cancelSubscription(user._id)}>
                                Cancel Subscription
                              </button>
                            ) : (
                              <button class="bg-transparent text-xs hover:bg-blue-500 text-blue-700 hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" onClick={() => renewSubscription(user._id)}>
                                Renew Subscription
                              </button>
                            )}
                          </td>
                          <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <a href="#" className="text-indigo-600 hover:text-indigo-900" onClick={() => {
                              setEditing(true)
                              setEditUserId(user._id)
                              setSection(1)
                            }}>
                              Edit
                            </a>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div className="bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6">
              <div className="flex-1 flex justify-between sm:hidden">
                <a
                  href="#"
                  className="relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
                >
                  Previous
                </a>
                <a
                  href="#"
                  className="ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
                >
                  Next
                </a>
              </div>
              <div className="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
                <div>
                  <p className="text-sm text-gray-700">
                    Showing <span className="font-medium">{1 + (page - 1) * limit}</span> to <span className="font-medium">{page * limit < details.count ? page * limit : details.count}</span> of{' '}
                    <span className="font-medium">{details.count}</span> results
                  </p>
                </div>
                <div>
                  <nav className="relative z-0 inline-flex rounded-md shadow-sm -space-x-px" aria-label="Pagination">
                    <a
                      onClick={() => {
                        if (page !== 1) {
                          setPage(page - 1)
                        }
                      }}
                      className="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50 cursor-pointer"
                    >
                      <span className="sr-only">Previous</span>
                      <ChevronLeftIcon className="h-5 w-5" aria-hidden="true" />
                    </a>
                    {/* Current: "z-10 bg-indigo-50 border-indigo-500 text-indigo-600", Default: "bg-white border-gray-300 text-gray-500 hover:bg-gray-50" */}
                    <a
                      aria-current="page"
                      className="z-10 bg-indigo-50 border-indigo-500 text-indigo-600 relative inline-flex items-center px-4 py-2 border text-sm font-medium"
                    >
                      {page}
                    </a>
                    <a
                      onClick={() => {
                        if (details.count > page * limit) {
                          setPage(page + 1)
                        }
                      }}
                      className="relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50 cursor-pointer"
                    >
                      <span className="sr-only">Next</span>
                      <ChevronRightIcon className="h-5 w-5" aria-hidden="true" />
                    </a>
                  </nav>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    )
  }
}
