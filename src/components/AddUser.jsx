import React, { useEffect, useState } from 'react';
import { ArrowLeftIcon } from '@heroicons/react/outline'

import { BASE_URL } from '../config';
import Modal from './Modal';

export default function AddUser({ editing, setEditing, editUserId, setSection }) {
  const [values, setValues] = useState({
    name: '',
    email: '',
    phone: '',
    paymentPlan: '',
    paymentCycle: '',
    amountDue: ''
  });
  const [error, setError] = useState(null);
  const [alert, setAlert] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    if (editing) {
      fetchUser()
    }
  }, [editing])

  // useEffect(() => {
  //   setEditing(false)
  // }, [])

  const deleteUser = () => {
    fetch(`${BASE_URL}/users/${editUserId}`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    }).then(response => {
      if (response.status === 200) {
        return response.json();
      }
    }).then(data => {
      setSection(0)
    });
  }

  const { name, email, phone, paymentCycle, paymentPlan, amountDue } = values

  const handleChange = (e) => {
    e.preventDefault();
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value
    });
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if (editing) {
      updateUser();
    } else {
      createUser();
    }
  }

  const fetchUser = () => {
    fetch(`${BASE_URL}/users/${editUserId}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
    }).then(response => {
      if (response.status !== 200) {
        setError(response.status + '===>' + response.statusText + '===>' + response.url)
      }
      setError(null);
      return response.json();
    }).then(data => {
      const { user } = data;
      setValues({
        name: user.name,
        email: user.email,
        phone: user.phone,
        paymentPlan: user.paymentPlan,
        paymentCycle: user.paymentCycle,
        amountDue: user.amountDue
      })
    });
  }

  const createUser = () => {
    fetch(`${BASE_URL}/users`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(values)
    }).then(response => {
      if (response.status !== 200) {
        setError(response.status + '===>' + response.statusText + '===>' + response.url)
      }
      setError(null);
      return response.json();
    }).then(data => {
      setAlert(true);
    });
  }

  const updateUser = () => {
    fetch(`${BASE_URL}/users/${editUserId}`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(values)
    }).then(response => {
      if (response.status !== 200) {
        setError(response.status + '===>' + response.statusText + '===>' + response.url)
      }
      setError(null);
      return response.json();
    }).then(data => {
      setAlert(true);
      setEditing(false);
      setSection(0);
    });
  }

  return (
    <div className="mt-10 sm:mt-0 p-16">
      <div className="mt-5 md:mt-0 md:col-span-2">
        <div className="m-5 flex items-center cursor-pointer" onClick={() => {
          setEditing(false)
          setSection(0)
        }}>
          <ArrowLeftIcon className="w-4 h-4 mr-3" />
          <p className="block text-sm font-medium text-gray-700">Go Back</p>
        </div>
        <form action="#" onSubmit={handleSubmit}>
          <div className="shadow overflow-hidden sm:rounded-md">
            <div className="px-4 py-5 bg-white sm:p-6">
              <div className="grid grid-cols-6 gap-6">
                <div className="col-span-6 sm:col-span-3">
                  <label htmlFor="name" className="block text-sm font-medium text-gray-700">
                    Full name
                  </label>
                  <input
                    type="text"
                    name="name"
                    id="name"
                    autoComplete="given-name"
                    value={name}
                    onChange={handleChange}
                    className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  />
                </div>

                <div className="col-span-6 sm:col-span-3">
                  <label htmlFor="email-address" className="block text-sm font-medium text-gray-700">
                    Email
                  </label>
                  <input
                    type="text"
                    name="email"
                    id="email-address"
                    autoComplete="email"
                    value={email}
                    onChange={handleChange}
                    className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  />
                </div>

                <div className="col-span-6 sm:col-span-3">
                  <label htmlFor="phone" className="block text-sm font-medium text-gray-700">
                    Phone Number
                  </label>
                  <input
                    type="number"
                    name="phone"
                    id="phone"
                    value={phone}
                    onChange={handleChange}
                    className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  />
                </div>

                <div className="col-span-6 sm:col-span-3">
                  <label htmlFor="plan" className="block text-sm font-medium text-gray-700">
                    Payment Plan
                  </label>
                  <input
                    type="text"
                    name="paymentPlan"
                    id="plan"
                    value={paymentPlan}
                    onChange={handleChange}
                    className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  />
                </div>

                {!editing && (
                  <>
                    <div className="col-span-6 sm:col-span-3">
                      <label htmlFor="cycle" className="block text-sm font-medium text-gray-700">
                        Payment Cycle
                      </label>
                      <select
                        id="cycle"
                        name="paymentCycle"
                        autoComplete="cycle"
                        value={paymentCycle}
                        onChange={handleChange}
                        className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                      >
                        <option value={1}>1 Month</option>
                        <option value={3}>3 Months</option>
                        <option value={6}>6 Months</option>
                        <option value={12}>12 Months / 1 Year</option>
                      </select>
                    </div>

                    <div className="col-span-6 sm:col-span-3">
                      <label htmlFor="amount-due" className="block text-sm font-medium text-gray-700 mb-1">
                        Amount Due
                      </label>
                      <input
                        type="number"
                        name="amountDue"
                        id="amount-due"
                        value={amountDue}
                        onChange={handleChange}
                        className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                      />
                    </div>
                  </>
                )}
              </div>
            </div>
            <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
              {editing ? (
                <>
                  <button
                    type="submit"
                    className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  >
                    Update User
                  </button>
                  <button
                    type="button"
                    className="ml-5 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
                    onClick={() => setModalOpen(true)}
                  >
                    Delete User
                  </button>
                </>
              ) : (
                <button
                  type="submit"
                  className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                >
                  Add User
                </button>
              )}
            </div>
          </div>
        </form>
        {alert && (
          <div class="mt-10 bg-blye-100 border border-blue-400 text-blue-700 px-4 py-3 rounded relative" role="alert">
            <strong class="font-bold">Success!</strong>
            {editing ? (
              <span class="ml-4 block sm:inline">User updated successfully.</span>
            ) : (
              <span class="ml-4 block sm:inline">User created successfully.</span>
            )}
            <span class="absolute top-0 bottom-0 right-0 px-4 py-3" onClick={() => setAlert(false)}>
              <svg class="fill-current h-6 w-6 text-blue-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" /></svg>
            </span>
          </div>
        )}
        <Modal open={modalOpen} setOpen={setModalOpen} deleteUser={deleteUser} />
      </div>
    </div>
  )
}
